import React, { Component } from 'react';

interface IButtonProps{
    buttonText: string
}

class Button extends Component<IButtonProps, {}> {
  render() {
    return (
      <div>
        {this.props.buttonText}
      </div>
    );
  }
}

export default Button;
