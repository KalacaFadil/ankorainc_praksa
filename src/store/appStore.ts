import { getSomething } from '../api/api'
import { observable, action } from 'mobx'

export default class AppStore {
	@observable
	public appInit: boolean = false
	
	@action
	public changeAppInit = (appInit: boolean = true) => {
	   this.appInit = appInit
	}

	@action
	public getSomething = async(
		handleSuccess: (p?: any) => any,
		handleError: (p?: any) => any
	) => {
		try{
			const response = await getSomething()
			handleSuccess(response)
		}catch(e){
			handleError(e)
		}
	}
}
