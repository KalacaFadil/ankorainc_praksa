import React from 'react'
import { inject } from 'mobx-react'
import AppStore from './store/appStore'

interface IProps {
  appStore?: AppStore
}

class StoreComponent<T = {}, S = {}> extends React.Component<T & IProps, S> {}
export default StoreComponent

export const injectAppStore = inject('appStore')