import React from 'react'
import { observer } from 'mobx-react'
import StoreComponent, { injectAppStore } from '../StoreComponent'
import { withRouter, RouteComponentProps } from 'react-router-dom'

@injectAppStore
@observer
class TestInfo extends StoreComponent<RouteComponentProps, { loaded: boolean }>{
  public state = {
    loaded: false
  }
  public componentDidMount(){
    this.props.appStore.getSomething(
      this.onGetSomethingSuccess,
      this.onGetSomethingFailure
    )
  }
  public render(){
    console.log("App init", this.props.appStore.appInit)
    return (
      <div>
        TestInfo
        <button onClick={() => this.props.history.push('/homepage')}>Go to homepage</button>
      </div>
    )
  }

  private onGetSomethingSuccess = (something: any) => {
    this.setState({
      loaded: true,
    })
    console.log(something)
  }

  private onGetSomethingFailure = (error: any) => {
    this.setState({
      loaded: true
    })
    console.error(error)
  }
}

export default withRouter(TestInfo)