import React from 'react'
import { observer } from 'mobx-react'
import StoreComponent, { injectAppStore } from '../StoreComponent'

@injectAppStore
@observer
class Homepage extends StoreComponent{
  public componentDidMount(){
    // this.props.appStore.changeAppInit()
  }
  public render(){
    console.log("App init", this.props.appStore.appInit)
    return (
      <div>
        Homepage
      </div>
    )
  }
}

export default Homepage