import axios from 'axios'
export const API_BASE_URL = 'http://localhost:3500'

export const getSomething = async () => {
  try {
    const response = await axios(
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `${API_BASE_URL}/api/v1/users`,
      }
    )
    const {status} = response
    if(status >= 200 && status < 300){
      return response.data
    }
  } catch (error) {
    console.log("ERR:", error.response)
    throw new Error(error)
  }
}