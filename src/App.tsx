import React, { Component } from 'react';
import AppStore from './store/appStore'
import { Provider } from 'mobx-react'
import Homepage from './pages/Homepage'
import TestInfo from './pages/TestInfo'

import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

class App extends Component {
  public appStore: AppStore = null

  constructor(props: any) {
    super(props)
    this.appStore = new AppStore()
  }

  render() {
    return (
      <BrowserRouter>
        <Provider
          appStore={this.appStore}
        >
          <Switch>
            <Route path='/homepage' exact component={Homepage} />
            <Route path='/test-info' exact component={TestInfo} />
            <Redirect from="/" to="/homepage" />
          </Switch>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
